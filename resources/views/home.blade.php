@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'titlePage' => __('Material Dashboard')])


@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Material Dashboard Heading</h4>
        <p class="card-category">Created using Roboto Font Family</p>
      </div>
      <div class="card-body">
        <div id="typography">
          <div class="card-title">
            <h2>Dashboard</h2>
          </div>
          <div class="row">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            You are logged in!
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
