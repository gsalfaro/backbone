<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>
            <li>
            <a href="https://importare.mx">
                {{ __('Importare MX') }}
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons"></i> by
        <a href="https://importare.mx" target="_blank">Importare MX</a>
        </div>
    </div>
</footer>