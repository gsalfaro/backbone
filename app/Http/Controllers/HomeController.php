<?php

namespace App\Http\Controllers;

use Conekta\Conekta;
use Conekta\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function configuracion()
    {
        $apiKey = config('conekta.api_key');
        $version= config('conekta.version');
        $locale = config('conekta.locale');
        Conekta::setApiKey($apiKey);
        Conekta::setApiVersion($version);
        Conekta::setLocale($locale);
        return;
    }

    public function test_pago(){

        $this->configuracion();
        $respuesta = [
            'error' => false,
            'error_mensaje' => null,
            'conekta' => []
        ];
        
        $orden['charges'][] = [
            'payment_method' => [
                'token_id' => 'testtesttest',
                'type'     => 'card'
            ]
        ];
        try {
            $respuesta['conekta'] = Order::create($orden);
        } catch (Exception $e) {
            $respuesta = [
                'error' => true,
                'error_mensaje' => $e->getMessage()
            ];
        }
        return $respuesta;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
